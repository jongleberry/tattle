
# tattle

[![NPM version][npm-image]][npm-url]
[![Build status][travis-image]][travis-url]
[![Test coverage][coveralls-image]][coveralls-url]
[![Dependency Status][david-image]][david-url]
[![License][license-image]][license-url]
[![Downloads][downloads-image]][downloads-url]
[![Gittip][gittip-image]][gittip-url]

Experimental chat system module using Redis for pub/sub, persisting to MongoDB.
Only streams are exposed - this module does not decide how the client receives the data.
Socket.IO? WebRTC? `¯\_(ツ)_/¯`

Implementation notes.

- Assumes there's a channel collection with all the channels.
  This module differentiates channels by their `_id`.
- Assumes there's a user collection with all users.
  Thus, "anonymous" users are not supported (give them their own `_id`).
  This module differentiates channels by their `_id`.
- Requires (or rather recommends) an entirely separate DB for storing channels.
  There's a collection per channel, with each `_id` being the name of the channel.

[gitter-image]: https://badges.gitter.im/jonathanong/tattle.png
[gitter-url]: https://gitter.im/jonathanong/tattle
[npm-image]: https://img.shields.io/npm/v/tattle.svg?style=flat-square
[npm-url]: https://npmjs.org/package/tattle
[github-tag]: http://img.shields.io/github/tag/jonathanong/tattle.svg?style=flat-square
[github-url]: https://github.com/jonathanong/tattle/tags
[travis-image]: https://img.shields.io/travis/jonathanong/tattle.svg?style=flat-square
[travis-url]: https://travis-ci.org/jonathanong/tattle
[coveralls-image]: https://img.shields.io/coveralls/jonathanong/tattle.svg?style=flat-square
[coveralls-url]: https://coveralls.io/r/jonathanong/tattle
[david-image]: http://img.shields.io/david/jonathanong/tattle.svg?style=flat-square
[david-url]: https://david-dm.org/jonathanong/tattle
[license-image]: http://img.shields.io/npm/l/tattle.svg?style=flat-square
[license-url]: LICENSE
[downloads-image]: http://img.shields.io/npm/dm/tattle.svg?style=flat-square
[downloads-url]: https://npmjs.org/package/tattle
[gittip-image]: https://img.shields.io/gratipay/jonathanong.svg?style=flat-square
[gittip-url]: https://gratipay.com/jonathanong/


var ObjectID = require('mongodb').ObjectID
var assert = require('assert')

var chat = require('..')

before(function () {
  return chat.db.mdb.connect
})

describe('Chat Client', function () {
  var user_id = new ObjectID()
  var channel_id = new ObjectID()
  var client = chat.client(user_id, channel_id)

  it('should publish its own message', function (done) {
    client.on('data', function (message) {
      assert(message.user_id.equals(user_id))
      assert(message.channel_id.equals(channel_id))
      assert.equal(message.message, 'LOL')
      done()
    })
    client.publish('LOL')
  })

  it('should persist that message', function () {
    return client.find().then(function (messages) {
      assert.equal(messages.length, 1)
      var message = messages[0]
      assert(message.user_id.equals(user_id))
      assert.equal(message.message, 'LOL')
    })
  })

  it('should stop broadcasting on destroy', function () {
    client.destroy()
    client.publish('KLjlkajsdfkljasdf')

    client.on('data', function () {
      throw new Error('boom')
    })
  })
})

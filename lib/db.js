
exports.redis = require('then-redis').createClient(
  process.env.REDIS_CHAT_URI || process.env.REDIS_URI || 'tcp://localhost:6379'
)

exports.mdb = require('mongodb-next')(
  process.env.MONGO_CHAT_URI || process.env.MONGO_URI || 'mongodb://localhost:27017'
)

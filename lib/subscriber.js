
var PassThrough = require('readable-stream/passthrough')
var ObjectID = require('mongodb').ObjectID
var redis = require('then-redis').createClient(
  process.env.REDIS_CHAT_URI || process.env.REDIS_URI || 'tcp://localhost:6379'
)

var stream = module.exports = new PassThrough({
  objectMode: true
})

// always emit messages, even who no one is listening
stream.on('data', function () {})

redis.psubscribe('*').catch(onerror)
redis.on('error', onerror)
redis.on('pmessage', function (pattern, channel, message) {
  message = JSON.parse(message)
  message._id = new ObjectID(message._id)
  message.user_id = new ObjectID(message.user_id)
  message.channel = channel
  message.channel_id = new ObjectID(channel)
  message.date = new Date(message.date)
  stream.write(message)
})

/* istanbul ignore next */
function onerror(err) {
  console.error(err.stack)
}

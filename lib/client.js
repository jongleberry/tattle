
var Transform = require('readable-stream/transform')
var ObjectID = require('mongodb').ObjectID
var destroyOnHWM = require('destroy-on-hwm')
var inherits = require('util').inherits

var subscriber = require('./subscriber')
var db = require('./db')

var mongodb = db.mdb
var redis = db.redis

inherits(Client, Transform)

module.exports = Client

/**
 * Each client is a stream associated between a user and a channel.
 *
 * TODO: maybe multiple channels? Not sure I see the point
 */

function Client(user_id, channel_id) {
  if (!(this instanceof Client)) return new Client(user_id, channel_id)

  Transform.call(this, {
    // messages are objects
    objectMode: true,
  })

  // store the ObjectID as well as string forms
  this.user_id = user_id
  this.user = user_id.toString('hex')
  this.channel_id = channel_id
  this.channel = channel_id.toString('hex')

  // pipe the source here
  subscriber.pipe(this, {
    end: false
  })

  // disconnect if the client is too slow
  destroyOnHWM(this)
}

/**
 * Each client simply filters all the messages by the channel.
 */

Client.prototype._transform = function (doc, NULL, cb) {
  if (doc.channel === this.channel) this.push(doc)
  cb()
}

/**
 * Publish a message to both redis and mongodb
 *
 * TODO: allow more than just a plain message
 */

Client.prototype.publish = function (message) {
  if (this.closed) return
  var _id = new ObjectID()
  // create the mongodb document
  var doc = {
    _id: _id,
    date: new Date(),
    user_id: this.user_id,
    message: message
  }
  // insert it
  mongodb.collection(this.channel).insert(doc).catch(onerror)
  // create the redis doc
  doc.id = _id.toHexString()
  doc.date = doc.date.toUTCString()
  doc.user = this.user
  redis.publish(this.channel, JSON.stringify(doc)).catch(onerror)
}

/**
 * Disconnect the client from the main source.
 */

Client.prototype.destroy = function () {
  if (this.closed) return
  subscriber.unpipe(this)
  this.closed = true
  this.emit('close')
}

/**
 * Search the history.
 * You should:
 *
 *   .sort({ _id: -1 })
 *   .offset()
 *   .limit()
 *
 */

Client.prototype.find = function () {
  var query = mongodb.collection(this.channel)
  return query.find.apply(query, arguments)
}

/* istanbul ignore next */
function onerror(err) {
  console.error(err.stack)
}
